#!/usr/bin/env python3

#======================================
# import modules
#======================================
import subprocess
import os
import sys

#======================================
# user defined parameters
#======================================
srcdir='postProcessing/postLines/'
tgtdir='postProcessing/postLines/'
srclines=[
    'lineHD_C21_U.xy',
    'lineHD_C22_U.xy',
    'lineHD_C23_U.xy',
    'lineHD_C24_U.xy',
    'lineHD_C25_U.xy',
    'lineHD_C26_U.xy',
    'lineHD_C31_U.xy',
    'lineHD_C32_U.xy',
    'lineHD_C33_U.xy',
    'lineHD_C34_U.xy',
    'lineHD_C35_U.xy',
    'lineHD_C36_U.xy',
    'lineHD_C37_U.xy',
    'lineHD_C38_U.xy',
    'lineHD_C39_U.xy'
    ]
tgtfiles=[
    'lineHD_C21_U',
    'lineHD_C22_U',
    'lineHD_C23_U',
    'lineHD_C24_U',
    'lineHD_C25_U',
    'lineHD_C26_U',
    'lineHD_C31_U',
    'lineHD_C32_U',
    'lineHD_C33_U',
    'lineHD_C34_U',
    'lineHD_C35_U',
    'lineHD_C36_U',
    'lineHD_C37_U',
    'lineHD_C38_U',
    'lineHD_C39_U'
    ]


#======================================
# main
#======================================

runDir = os.getcwd()

# Check if output exist. If not, create it
if os.path.isdir("output")==True:
    pass
else:
    os.mkdir("output")

# srclines and tgtfiles must have the same lenght
if len(srclines)!=len(tgtfiles):
    sys.exit('Warning: srclines and tgtfiles must have the same lenght. exit()')
else:
    pass

# start jobs on brutus
for i in range(len(srclines)):
    proc = subprocess.Popen('bsub -o output/lsf.foamPL2PP_'+str(srclines[i])+' -W 1:00 -n 1 ./foamPL2PP.py -srcdir '+srcdir+' -srcline '+srclines[i]+' -tgtfile '+tgtdir+tgtfiles[i]+'', stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    print(out)

