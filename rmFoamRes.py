#!/cluster/apps/python/2.7.2/x86_64/bin/python

#======================================
# user defined parameter
#======================================


#======================================
# import modules
#======================================
import os
import sys
import argparse

sys.path.insert(0,'/cluster/home04/baug/marcelv/OpenFOAM/openfoam_scripts/FoamTime')
from FoamTime import FoamTime

#======================================
# reading passed arguments
#======================================
parser = argparse.ArgumentParser(description='remove OpenFOAM timeStep')
parser.add_argument('-ts','--timestep', type=str, nargs='+', action="store", dest="timestep", help='list of openFoam time step.')
args = parser.parse_args()

workdir = os.getcwd()

#======================================
# main
#======================================
if(args.timestep == None):
    sys.exit('Optional argument -ts is not optional...')
else:
    pass
    
for ts_str in args.timestep:
    if(    ts_str!='25' 
        or ts_str!='26' 
        or ts_str!='27' 
        or ts_str!='28' 
        or ts_str!='29' 
        or ts_str!='30'
        or ts_str!='31'
        or ts_str!='32'
        or ts_str!='33'
        or ts_str!='34'
        or ts_str!='35'
       ):
        print '*** Working with time '+ts_str+' ***' 
        tstep = FoamTime(ts_str)
        tstep.rmDecomposedTime()
        