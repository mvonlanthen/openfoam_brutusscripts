#!/bin/bash

# Check if output exist. If not, create it
mkdir -p output

# run PreProcessing
bsub -J checkMesh -oo output/checkMesh.out -R "rusage[mem=4096]" checkMesh
bsub -J decomposePar -w "done(checkMesh)" -oo output/decomposePar.out -R "rusage[mem=4096]" decomposePar -force
bsub -J solverRun  -w "done(decomposePar)" -o output/solver.out -W 48:00 -n 120 mpirun -np 120 pimpleFoam -parallel