#! /usr/bin/env python3

# math modules
import numpy as np

h = 0.05

# cube min max box
c11 = np.array([[0*h,0.0,0.0],[1*h,h,h]])
c12 = np.array([[4*h,0.0,0.0],[5*h,h,h]])
c13 = np.array([[8*h,0.0,0.0],[9*h,h,h]])

c21 = np.array([[2*h,-h,0.0],[3*h,0.0,h]])
c22 = np.array([[6*h,-h,0.0],[7*h,0.0,h]])
c23 = np.array([[10*h,-h,0.0],[11*h,0.0,h]])

c31 = np.array([[0*h,-0.1,0.0],[1*h,-h,h]])
c32 = np.array([[4*h,-0.1,0.0],[5*h,-h,h]])
c33 = np.array([[8*h,-0.1,0.0],[9*h,-h,h]])

c41 = np.array([[2*h,3*-h,0.0],[3*h,2*-h,h]])
c42 = np.array([[6*h,3*-h,0.0],[7*h,2*-h,h]])
c43 = np.array([[10*h,3*-h,0.0],[11*h,2*-h,h]])

c51 = np.array([[0*h,4*-h,0.0],[1*h,3*-h,h]])
c52 = np.array([[4*h,4*-h,0.0],[5*h,3*-h,h]])
c53 = np.array([[8*h,4*-h,0.0],[9*h,3*-h,h]])



# inflate cubes
def inflateCube(cXX,inflate):
    cXXinflate = np.zeros(cXX.shape)
    cXXinflate[0,:] = cXX[0,:]-inflate
    cXXinflate[1,:] = cXX[1,:]+inflate
    return cXXinflate
    
def printSnappyBoxCoor(cXX):
    #boxC31Level04
    #{
    #    type searchableBox;
    #    min (-0.00625 -0.10625 -0.00625);
    #    max ( 0.05625 -0.04375  0.05625);
    #}
    print('    min ('+str(cXX[0,0])+' '+str(cXX[0,1])+' '+str(cXX[0,2])+');')
    print('    max ('+str(cXX[1,0])+' '+str(cXX[1,1])+' '+str(cXX[1,2])+');')


inflate = 1*(h/64)
cXX =      [c11,c12,c13,  c21,c22,c23,  c31,c32,c33,  c41,c42,c43,  c51,c52,c53]
cXXname =  ['c11','c12','c13','c21','c22','c23','c31','c32','c33','c41','c42','c43','c51','c52','c53']

for i in range(len(cXX)):
    
    print('box'+cXXname[i]+'Level05    //refinement up to 128 cells/h')
    print('{')
    print('    type searchableBox;')
    cXXinfl = inflateCube(cXX[i],inflate)
    printSnappyBoxCoor(cXXinfl)
    print('}')


print('=======')
topInfl = 4*(h/32)
topBox = np.array([[0.0,-4*h,h],[11*h,1*h,h]])
topBoxInfl = inflateCube(topBox,topInfl)
printSnappyBoxCoor(topBoxInfl)
