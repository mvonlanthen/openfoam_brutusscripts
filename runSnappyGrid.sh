#!/bin/bash

#=================================================================
# User parameters
#=================================================================
# included stl objects. name of the stl-file without extention
sltObj=(
        c11
        c12
        c13
        c21
        c22
        c23
        c31
        c32
        c33
        c41
        c42
        c43
        c51
        c52
        c53
    )


cleanObj=1     # clean *.obj files created by surfaceFeatureExtract (1=yes / 0=no)
runBlockMesh=1  #run blockMesh, yes (=1) or no (=0)?


#=================================================================
runDir=$(pwd)

echo clean folder constant
cd constant/polyMesh
rm -rf $(ls * | grep -v blockMeshDict)
cd ..
rm -rf extendedFeatureEdgeMesh

if [ $runBlockMesh==1 ]
then
    echo run blockMesh
    cd $runDir
    blockMesh > output/blockMesh.out 2>&1 
fi
    
echo create *.eMesh files  #useful to catch correctly edges
for i in "${sltObj[@]}"
do
    surfaceFeatureExtract -includedAngle 150 -writeObj constant/triSurface/$i.stl $i  > output/surfaceFeatureExtract_$i.out 2>&1
done

if [ $cleanObj==1 ]
then
    echo clean obj files
    rm *.obj
fi

echo run snappyHexMesh -overwrite
snappyHexMesh -overwrite > output/snappyHexMesh.out 2>&1