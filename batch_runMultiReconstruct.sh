#!/bin/bash


timeStep=( 
0
0.02
0.04
    )


# Check if output exist. If not, create it
mkdir -p output

# run reconstruction for each timestep
for i in "${timeStep[@]}"
do
        bsub -J reconstructPar_$i -oo output/lsf.reconstructPar_$i -W 2:00 -R "rusage[mem=4096]" reconstructPar -time $i
done
