#!/bin/sh


# echo copy initial condition from 0.org to 0
# rm 0/*
# rm 0.org/*~
# cp 0.org/* 0/
# 
# echo run blockMesh
# blockMesh > output/blockMesh.out 2>&1
# 
# echo run checkMesh
# checkMesh > output/checkMesh.out 2>&1
# 
# echo run setFields
# checkMesh > output/setFields.out 2>&1

# echo run decomposePart -force
# decomposePar -force > output/decomposePar.out 2>&1

echo parallel run buoyantBoussinesqTurbulentPimpleFoam
mpirun -np 4 buoyantBoussinesqTurbulentPimpleFoam -parallel > output/solver.out 2>&1
