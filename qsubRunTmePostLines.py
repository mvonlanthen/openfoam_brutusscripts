#!/usr/bin/env python

import os
import glob
import subprocess

workdir = os.getcwd()

def qsubJob():
    pass
    

#if os.environ['OMPI_COMM_WORLD_RANK']=='0': # for OpenMPI
#if os.environ['PMI_RANK']=='0': # for mpich
if os.environ['PMI_ID']=='0': # for mvapich2
    proc_path = glob.glob(workdir+'/proc*')
    os.chdir(proc_path[0])
    dirlist = filter(os.path.isdir, os.listdir(proc_path[0]))
    dirlist.sort(key=lambda x: os.path.getmtime(x)) 
    try:
        dirlist.remove('constant')
    except:
        pass
    try:
        dirlist.remove('0')
    except:
        pass
    #print dirlist
    if len(dirlist)>1:
        os.chdir(workdir)
        print 'reconstruction, ziping and cleaning submitted for time '+dirlist[-2]+''
        proc = subprocess.Popen('qsub batch_saveFoamRes.sh -v ts='+dirlist[-2]+'', stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
    else:
        print 'reconstruction, ziping and cleaning will start at the next output time'
