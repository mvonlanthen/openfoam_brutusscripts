#!/usr/bin/env python2

#======================================
# user defined parameter
#======================================

#======================================
# import modules
#======================================
import os
import sys
import argparse

sys.path.insert(0,'/cluster/home/marcelv/OpenFOAM/OpenFOAM_scripts/FoamTime')
from FoamTime import FoamTime

#======================================
# reading passed arguments
#======================================
parser = argparse.ArgumentParser(description='reconstruct, zip and clean OpenFOAM time step')
parser.add_argument('-time',
                    type=str,
                    nargs='+',
                    action="store",
                    required=True,
                    dest="time",
                    help='list of openFoam time step.'
                    )
parser.add_argument('-ef',
                    type=str,
                    nargs='+',
                    action="store",
                    dest="excludeFields",
                    default=None,
                    help='Exclude fields from being zipped.'
                    )
parser.add_argument('-region',
                    type=str,
                    nargs='+',
                    action="store",
                    dest="region",
                    required=True,
                    help='define the processed regions. Standard region: region0'
                    )
parser.add_argument('-delrec',
                    dest="delrec",
                    action="store_true",
                    default=False,
                    help='delete reconstructed time. Default: False. If zipping failed: always False'
                    )
parser.add_argument('-deldec',
                    dest="deldec",
                    action="store_true",
                    default=False,
                    help='delete decomposed time. Default: False. If zipping failed: always False'
                    )
args = parser.parse_args()

workdir = os.getcwd()

#======================================
# main
#======================================
excludeFields = []
if (args.excludeFields==None):
    pass
else:
    excludeFields = args.excludeFields
    
for ts_str in args.time:        
    tstep = FoamTime(ts_str,excludeZipList=excludeFields)   
    for i in range(len(args.region)):
        recDone = tstep.reconstructTime(region=args.region[i], OFout=True)
        if recDone==False:
            sys.exit(tstep.printInfoTime(printInfo='reconstruction failed: Exit.'))
    zipDone = tstep.zipTime()
    if zipDone==True and args.delrec:
        tstep.rmReconstructedTime()
    if zipDone==True and args.deldec:
        tstep.rmDecomposedTime()
    if zipDone==False:
        sys.exit(tstep.printInfoTime(printInfo='zip failed: Exit.'))
        