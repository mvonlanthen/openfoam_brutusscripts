#!/bin/sh

mkdir -p output

echo sample -latestTime -dict ../postProcessingDict/sampleDict_surfaces 
sample -latestTime -dict ../postProcessingDict/sampleDict_surfaces > output/sample_surfaces.out 2>&1

#echo sample -latestTime -dict ../postProcessingDict/sampleDict_lines
#sample -latestTime -dict ../postProcessingDict/sampleDict_lines > output/sample_lines.out 2>&1

#echo sample -latestTime -dict ../postProcessingDict/sampleDict_extraLines00
#sample -latestTime -dict ../postProcessingDict/sampleDict_extraLines00 > output/sample_extraLines00.out 2>&1
