#!/bin/bash

# run pimpleFoam in chain
bsub -J job0 -o output/lsf.solver -W 36:00 -n 64 mpirun -np 64 pimpleFoam -parallel
bsub -J job1 -w "ended(job0)" -o output/lsf.solver -W 36:00 -n 64 mpirun -np 64 pimpleFoam -parallel
bsub -J job2 -w "ended(job1)" -o output/lsf.solver -W 36:00 -n 64 mpirun -np 64 pimpleFoam -parallel