#!/bin/bash

# Check if output exist. If not, create it
mkdir -p output

bsub -J solverRun -o output/solver_run00.out -W 24:00 -n 168 mpirun -np 168 buoyBousTurbulentPimpleFoam -parallel
# bsub -J solverRun -w "done(decomposePar)" -o output/solver_run00.out -W 24:00 -n 168 mpirun -np 168 buoyBousTurbulentPimpleFoam -parallel
