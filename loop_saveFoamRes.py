#!/usr/bin/env python3


#======================================
# import modules
#======================================
import os
import sys
import subprocess
import argparse


#======================================
# user defined parameter
#======================================
tsList = [12,14,16]


#======================================
# functions
#======================================


#======================================
# main
#======================================
proc = subprocess.Popen(('bsub '
                             '-J saveFoamRes_'+str(tsList[0])+' '
                             '-oo output/saveFoamRes_'+str(tsList[0])+'.out '
                             '-R "rusage[mem=4096]" '
                             './saveFoamRes.py -delrec -deldec -time '+str(tsList[0])+' -region region0'),
                            stdout=subprocess.PIPE,
                            shell=True)
while proc.poll() is None:
    line = proc.stdout.readline()
    print(line)
print(proc.stdout.read())


for i in range(1, len(tsList)):
    proc = subprocess.Popen(('bsub '
                             '-J saveFoamRes_'+str(tsList[i])+' '
                             '-oo output/saveFoamRes_'+str(tsList[i])+'.out '
                             '-w "ended(saveFoamRes_'+str(tsList[i-1])+')" '
                             '-R "rusage[mem=4096]" '
                             './saveFoamRes.py -delrec -deldec -time '+str(tsList[i])+' -region region0'),
                            stdout=subprocess.PIPE,
                            shell=True)
    while proc.poll() is None:
        line = proc.stdout.readline()
        print(line)
    print(proc.stdout.read())
