#!/bin/sh

mkdir -p output

CWD=$(pwd)

probeNames=(
    probesCL1
    probesCL2
    probesCL3
    probesCL4
    probesCL5
    probesCL6
    probesCL7
    probesCL8
    probesInL1
    probesInL2
    probesInL3
)


for i in "${probeNames[@]}"
do
    echo "saveOFprobesToH5.py -probename $i"
    cd postProcessing/$i
    mv U.h5 U_raw_old.h5
    cd $CWD
    
    saveOFprobesToH5.py -probename $i -var U -keyrange full -overwrite      
    cd postProcessing/$i 
    mv U.h5 U_full.h5
    cd $CWD
    
    saveOFprobesToH5.py -probename $i -var U -keyrange raw -overwrite   
    cd postProcessing/$i
    mv U.h5 U_raw.h5
    cd $CWD
done



