#!/bin/bash


# User defined parameters
timeList=(
92.23648
92.33648
93.23648
94.13648 
)


for ts in "${timeList[@]}"
do
    echo "run 'saveFoamRes_local.py -delrec -deldec -time $ts -region region0'"
    ./saveFoamRes_local.py -delrec -deldec -time $ts -region region0 > output/saveFoamRes_$ts.out 2>&1
done

