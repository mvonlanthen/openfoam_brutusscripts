#!/bin/bash


# User defined parameters
probeName=(
probeslx2
probeslz1
probeslz3
probeslz4
probeslz5
probeslz6
probeslz8
)

for i in "${probeName[@]}"
do
    echo "=========================================="
    saveOFprobesToH5.py -probename $i -var U T kMean  TMean  UMean  UPrime2Mean  -keyrange full -rename full -overwrite
done

for i in "${probeName[@]}"
do
    echo "=========================================="
    saveOFprobesToH5.py -probename $i -var U T kMean  TMean  UMean  UPrime2Mean  -keyrange raw -rename raw -overwrite
done

