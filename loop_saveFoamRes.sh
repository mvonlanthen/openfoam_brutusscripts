#!/bin/bash

#======================================
# user defined parameter
#======================================
timeList=(

)

#======================================
# main
#======================================
for ts in "${timeList[@]}"
do
    echo "submit 'saveFoamRes.py -delrec -deldec -time $ts -region region0'"
    bsub -J saveFoamRes_$ts -oo output/saveFoamRes_$ts.out -R "rusage[mem=4096]" ./saveFoamRes.py -delrec -deldec -time $ts -region region0

done

