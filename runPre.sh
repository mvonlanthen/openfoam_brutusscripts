#!/bin/sh


# echo copy initial condition from 0.org to 0
# rm 0/*
# rm 0.org/*~
# cp 0.org/* 0/

# echo run runSnappyGrid.sh
# ./runSnappyGrid.sh > output/runSnappyGrid.out 2>&1

# echo run checkMesh
# checkMesh > output/checkMesh.out 2>&1

echo run decomposePart -force
decomposePar -force > output/decomposePar.out 2>&1