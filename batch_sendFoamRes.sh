#!/bin/bash

# Check if output exist. If not, create it
mkdir -p output

timeList=(
    10
)

for i in "${timeList[@]}"
do
    bsub -oo output/lsf.sendFoamRes_$i -W 1:00 -n 1 -R "rusage[mem=8192]"  ./sendFoamRes.py -ts $i
done