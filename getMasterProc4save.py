#!/cluster/apps/python/2.7.2/x86_64/bin/python

import os
import glob
import subprocess

workdir = os.getcwd()


#======================================
# get sorted list of time dirs in 
# processor0. On master process only
#======================================
if os.environ['OMPI_COMM_WORLD_RANK']=='0':
    proc_path = glob.glob(workdir+'/proc*')
    os.chdir(proc_path[0])
    dirlist = filter(os.path.isdir, os.listdir(proc_path[0]))
    #dirlist_path = [os.path.join(proc_path[0], f) for f in dirlist] #add path to each time folder
    dirlist.sort(key=lambda x: os.path.getmtime(x)) 
    dirlist.remove('constant') 
    #print dirlist
    if len(dirlist)>1:
        os.chdir(workdir)
        print 'reconstruction,zipping and cleaning submitted with \'bsub ./sendFoamRes.py -ts '+dirlist[-2]+'\''
        proc = subprocess.Popen('bsub -oo output/lsf.saveFoamRes_'+dirlist[-2]+' -R "rusage[mem=4096]" ./saveFoamRes.py -ts '+dirlist[-2]+'', stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
    else:
        print 'reconstruction,zipping and cleaning will start at the next write time'