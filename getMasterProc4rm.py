#!/cluster/apps/python/2.7.2/x86_64/bin/python

import os
import glob
import subprocess

workdir = os.getcwd()


#======================================
# get sorted list of time dirs in 
# processor0. On master process only
#======================================
if os.environ['OMPI_COMM_WORLD_RANK']=='0':
    proc_path = glob.glob(workdir+'/proc*')
    os.chdir(proc_path[0])
    dirlist = filter(os.path.isdir, os.listdir(proc_path[0]))
    dirlist.sort(key=lambda x: os.path.getmtime(x)) 
    dirlist.remove('constant') 
    #print dirlist
    if len(dirlist)>1:
        os.chdir(workdir)
        print 'Delete OpenFOAM timestep.'
        proc = subprocess.Popen('bsub -oo output/lsf.rmFoamRes_'+dirlist[-2]+' ./rmFoamRes.py -ts '+dirlist[-2]+'', stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
    else:
        print 'Delete OpenFOAM timestep will start at the next write time.'